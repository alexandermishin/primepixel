#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>


using namespace std;


template <typename T>
class Stack
{
public:
	Stack();
	~Stack();

	T getData();
	T getDataNext();

	void setDataNext(T data);
	void deleteFirst();
	void clear();
	bool isEmpty();
	void push(T data);
	T pop();
private:
	template <typename T>
	struct Node
	{
		T data;
		Node<T>* pNext;
		Node(T data = T(), Node<T>* pNext = nullptr)
		{
			this->data = data;
			this->pNext = pNext;
		}
	};
	Node<T>* head;
};

template<typename T>
Stack<T>::Stack()
{
	head = NULL;
}

template<typename T>
Stack<T>::~Stack()
{
	clear();
}

template<typename T>
T Stack<T>::getData()
{
	return head->data;
}

template<typename T>
T Stack<T>::getDataNext()
{
	return head->pNext->data;
}

template<typename T>
void Stack<T>::setDataNext(T data)
{
	head->pNext->data = data;
}

template<typename T>
void Stack<T>::deleteFirst()
{
	Node<T>* temp = head;
	head = head->pNext;
	delete temp;

}

template<typename T>
void Stack<T>::clear()
{
	while (head)
	{
		deleteFirst();
	}
}

template<typename T>
bool Stack<T>::isEmpty()
{
	return !head;
}

template<typename T>
void Stack<T>::push(T data)
{
	head = new Node<T>(data, head);
}

template<typename T>
T Stack<T>::pop()
{
	T data;
	Node<T>* temp = this->head;
	data = temp->data;
	head = head->pNext;
	delete temp;
	return data;
}

class Operator
{
public:
	Operator();
	Operator(string exp);

	int indentityTheSign(char temp);

	void setExpression(string& exp);
	void workWithExpression(char tmp, string& fnl, Stack<char>& stk);
	void workWithLetter(char tmp, string& fnl);
	void convertExpression(string& fnl, Stack<char>& stk);
	void workWithNumber(char tmp, string& fnl);
	void workWithPostNumber(char tmp, Stack<double>& stk);
	void workWithPostSign(char tmp, Stack<double>& stk);
	void calculateExpression(string& expr, Stack<double>& stk);
	void workWithSum(Stack<double>& stk);
	void workWithSub(Stack<double>& stk);
	void workWithMult(Stack<double>& stk);
	void workWithDiv(Stack<double>& stk);
	void workWithPow(Stack<double>& stk);

	bool isNumber(char temp);
	bool isLetter(char temp);
	bool isSign(char tmp);

private:

	string expression;
};

Operator::Operator()
{
	expression = "";
}

Operator::Operator(string exp)
{
	expression = exp;
}

int Operator::indentityTheSign(char temp)
{
	switch (temp)
	{
	case '(':
		return 1;
	case ')':
		return 2;
	case '+':
		return 3;
	case '-':
		return 3;
	case '*':
		return 4;
	case '/':
		return 4;
	case '^':
		return 5;
	default:
		return 0;
	}
}

void Operator::setExpression(string& exp)
{
	expression = exp;
}

void Operator::workWithExpression(char tmp, string& fnl, Stack<char>& stk)
{
	switch (indentityTheSign(tmp))
	{
	case 1: // '('
		stk.push(tmp);
		break;
	case 2: // ')'
		while ((!stk.isEmpty()) && (stk.getData() != '('))
		{
			fnl = fnl + stk.pop() + " ";
		}
		stk.deleteFirst();
		break;
	case 3: // '+'| '-'
		if (stk.isEmpty())
		{
			stk.push(tmp);
		}
		else
		{
			if ((stk.getData() == '*') || (stk.getData() == '/') || (stk.getData() == '^'))
			{
				while ((!stk.isEmpty()) && (stk.getData() != '('))
				{
					fnl = fnl + stk.pop() + " ";
				}
				stk.push(tmp);
			}
			else
			{
				stk.push(tmp);
			}
		}
		break;
	case 4: // '*' | '/'
		if (stk.isEmpty())
		{
			stk.push(tmp);
		}
		else
		{
			if (stk.getData() == '^')
			{
				while ((!stk.isEmpty()) && (stk.getData() != '('))
				{
					fnl = fnl + stk.pop();
				}
				stk.push(tmp);
			}
			else
			{
				stk.push(tmp);
			}
		}
		break;
	case 5: // '^'
		stk.push(tmp);
		break;
	case 0:// other
		workWithLetter(tmp, fnl);
		workWithNumber(tmp, fnl);
	}
}

void Operator::convertExpression(string& fnl, Stack<char>& stk)
{
	fnl = "";
	for (int i = 0; i < expression.length(); i++)
	{
		workWithExpression(expression[i], fnl, stk);
	}
	while (!stk.isEmpty())
	{
		fnl = fnl + stk.pop() + " ";
	}
}

void Operator::workWithNumber(char tmp, string& fnl)
{
	if (isNumber(tmp))
	{
		fnl = fnl + tmp + " ";
	}
}

void Operator::workWithPostNumber(char tmp, Stack<double>& stk)
{
	if (isNumber(tmp))
	{
		int i = tmp - 48;
		stk.push(i);
	}
}

void Operator::workWithPostSign(char tmp, Stack<double>& stk)
{
	if (isSign(tmp))
	{
		switch (tmp)
		{
		case '+':
			workWithSum(stk);
			break;
		case '-':
			workWithSub(stk);
			break;
		case '*':
			workWithMult(stk);
			break;
		case '/':
			workWithDiv(stk);
			break;
		case '^':
			workWithPow(stk);
			break;
		}

	}
}

void Operator::calculateExpression(string& expr, Stack<double>& stk)
{
	for (int i = 0; i < expr.length(); i++)
	{
		workWithPostNumber(expr[i], stk);
		workWithPostSign(expr[i], stk);
	}
}

void Operator::workWithSum(Stack<double>& stk)
{
	stk.setDataNext(stk.getDataNext() + stk.getData());
	stk.deleteFirst();
}

void Operator::workWithSub(Stack<double>& stk)
{
	stk.setDataNext(stk.getDataNext() - stk.getData());
	stk.deleteFirst();
}

void Operator::workWithMult(Stack<double>& stk)
{
	stk.setDataNext(stk.getDataNext() * stk.getData());
	stk.deleteFirst();
}

void Operator::workWithDiv(Stack<double>& stk)
{
	stk.setDataNext(stk.getDataNext() / stk.getData());
	stk.deleteFirst();
}

void Operator::workWithPow(Stack<double>& stk)
{
	float temp = 1;
	for (int i = 0; i < stk.getData(); i++)
	{
		temp = temp * stk.getDataNext();
	}
	stk.setDataNext(temp);
	stk.deleteFirst();
}


void Operator::workWithLetter(char tmp, string& fnl)
{
	if (isLetter(tmp))
	{
		fnl = fnl + tmp + " ";
	}
}

bool Operator::isNumber(char temp)
{
	if ((temp > 47) && (temp < 59))
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool Operator::isLetter(char temp)
{
	if ((temp> 96) && (temp < 123))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Operator::isSign(char tmp)
{
	if (((tmp > 41 ) && (tmp < 48)) || (tmp == 94))
	{
		return true;
	}
	else
	{
		return false;
	}
}


int main()
{
	Stack<char> stk;
	Stack<double> stk1;
	string expr = "2^4+3*6/(7*8+1)+4";
	string expr1 = "2^9";
	string expr2 = "2^2+4*2";
	string fnl;
	Operator op;
	op.setExpression(expr);
	cout << "Infix expression: " << expr << "\n";
	op.convertExpression(fnl, stk);
	cout << "Postfix expression: " << fnl << "\n";
	op.calculateExpression(fnl, stk1);
	cout << "Result: " << stk1.getData() << "\n";
	op.setExpression(expr1);
	cout << "Infix expression: " << expr1 << "\n";
	op.convertExpression(fnl, stk);
	cout << "Postfix expression: " << fnl << "\n";
	op.calculateExpression(fnl, stk1);
	cout << "Result: " << stk1.getData() << "\n";
	op.setExpression(expr2);
	cout << "Infix expression: " << expr2 << "\n";
	op.convertExpression(fnl, stk);
	cout << "Postfix expression: " << fnl << "\n";
	op.calculateExpression(fnl, stk1);
	cout << "Result: " << stk1.getData() << "\n";
}
